let photos = [
    ["public/fantasy-2049567_1920.jpg", "public/pexels-roberto-shumski-1903702.jpg", "public/nature-3082832_1920.jpg","public/nature-3082832_1920.jpg","public/nature-3082832_1920.jpg"],
    ["public/fantasy-2049567_1920.jpg", "public/pexels-roberto-shumski-1903702.jpg", "public/nature-3082832_1920.jpg","public/nature-3082832_1920.jpg","public/nature-3082832_1920.jpg"],
    ["public/fantasy-2049567_1920.jpg", "public/pexels-roberto-shumski-1903702.jpg", "public/nature-3082832_1920.jpg","public/nature-3082832_1920.jpg","public/nature-3082832_1920.jpg"]
    
];
let titles =[
    ["title 1", "title 2", "title 3", "title 4", "title 5"],
    ["title 1", "title 2", "title 3", "title 4", "title 5"],
    ["title 1", "title 2", "title 3", "title 4", "title 5"]
];

function loadPhoto(album){
    for(let i = 0; i < photos.length; i++){
        let row = document.createElement("div");
        row.classList = "row d-flex justify-content-center";
        for(let j = 0; j < photos[i].length;j++){
            let photoElement = document.createElement("div");
            photoElement.classList = "col-10 col-sm-10 col-md-6 m-2 col-xl-2 p-2 rounded-3 img floating-label photo-card";
            photoElement.setAttribute("data-tooltip", titles[i][j])
            let image = document.createElement("img");
            image.src = photos[i][j]
            image.classList = "w-100 rounded-3";
            photoElement.append(image);
            row.append(photoElement);
        }
        album.append(row)
    }
}

let photoAlbumElement = document.getElementById("photoAlbum");
loadPhoto(photoAlbumElement)
