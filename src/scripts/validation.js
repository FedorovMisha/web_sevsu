function focusTo(message, element){
    alert(message);
    if(element != null && element != undefined)
        element.focus();
    return false;
}

function contactFormValidation(){
    let form = document.getElementById("contact_form")
    let valid = true;

    const validateFullName = (value) => value.toString().split(' ').length == 3

    if(form.fullname.value === "") return focusTo("Поле не должно быть пустым", form.fullName)
    if(!validateFullName(form.fullname.value)) return focusTo("Введите корректную ФИО")
    if(form.telephone.value === "")
        return focusTo("Поле не должно быть пустым", form.telephone)
    
    let expr = /^(\+[73][1-9]*)/;

    if(form.telephone.value.match(expr) == null || form.telephone.value.toString().includes(' '))
        return focusTo("Введен не корректный номер телефона", form.telephone)
    
    if(form.email.value === "") return focusTo("Поле email не должно быть пустым", form.email)
    if(form.age.selectedIndex === 0) return focusTo("Выберите ваш возраст", form.age)

    return valid;
}


function mathTestFormValidation(){
    let form = document.getElementById("mathTestForm")
    let valid = true;
    if(form.fullName.value === "") 
        return focusTo("Поле не должно быть пустым", form.fullName)

    if(form.group.selectedIndex === 0)
        return focusTo("Выберите другое значение", form.group)

    if(form.task1.value === "")
        return focusTo("Поле не должно быть пустым", form.task1)

    let words = form.task1.value.split(' ').filter(function(i){return i})
    if(words.length < 30)
        return focusTo("Количество слов в ответе не менее 30", form.task1);

    let items = Array.from(form.task2)
    console.log(items.every((el) => el.checked === false))

    if(items.every((element) => element.checked === false) === true)
        return focusTo("Выберите значение для теста №2");

    if(form.task3.selectedIndex === 0)
        return focusTo("Выберите другое значение", form.task3) 

    return valid;
}

function trimText(event){
    let input = event.target;
    input.value = input.value.toString().trim();
}
